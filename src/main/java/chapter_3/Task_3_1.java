package chapter_3;


/**
 * Napisz program, który za pomocą instrukcji for dla danych
 * wartości x zmieniających się od 0 do 10 oblicza wartość
 * funkcji y = 3x.
 **/
public class Task_3_1 {

    public void main(){

        int y = 0;

        for (int x = 0; x <= 10; x++) {
            System.out.print(" x= " + x);
            System.out.println(" y= " + 3 * x);
        }
    }
}
