package chapter_3;

/**
 * Napisz program, który za pomocą instrukcji while sumuje
 * liczby nieparzyste w przedziale od 1 do 100.
 */
public class Task_3_15 {

    public void main(){

        int i =1;
        int sum=0;

        while (i<+100){

            if (i%2==1){
                sum+=i;
            }
            i++;
        }
        System.out.println("suma: "+sum);


    }
}
