package chapter_3;

/**
 * Napisz program, który za pomocą instrukcji while dla danych
 * wartości x zmieniających się od 0 do 10 oblicza wartość
 * funkcji y = 3x.
 **/

public class Task_3_3 {

    public void main() {

        int x = 0;
        int y = 0;

        while (x <= 10) {
            y = 3 * x;
            System.out.print("x = " + x);
            System.out.println(" y = " + 3 * x);
            x++;
        }
    }
}
