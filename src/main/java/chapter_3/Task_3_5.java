package chapter_3;

/**
 * Napisz program, który za pomocą instrukcji do ... while wyświetla
 * liczby całkowite od 1 do 20.
 **/

public class Task_3_5 {

    public void main() {

        int x = 1;

        do {
            System.out.println(x);
            x++;
        } while (x <= 20);
    }
}
