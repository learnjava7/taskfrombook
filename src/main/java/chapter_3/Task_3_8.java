package chapter_3;

/**
 * Napisz program, który korzystając z instrukcji do ... while,
 * sumuje liczby całkowite od 1 do 100.
 **/

public class Task_3_8 {

    public void main() {

        int i = 1;
        int sum = 0;

        do {
            sum += i;
            i++;
        } while (i <= 100);

        System.out.println("suma: " + sum);

    }
}
