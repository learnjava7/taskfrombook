package chapter_3;

/**
 * Napisz program, który za pomocą instrukcji for sumuje liczby
 * parzyste z przedziału od 1 do 100.
 */
public class Task_3_10 {

    public void main() {

        int sum = 0;

        for (int i = 1; i <= 100; i++) {

            if (i % 2 == 0) {
                sum += i;
            }
        }
        System.out.println("suma: " + sum);
    }
}
