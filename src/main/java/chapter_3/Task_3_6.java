package chapter_3;

/**
 * Napisz program, który za pomocą instrukcji while wyświetla
 * liczby całkowite od 1 do 20.
 **/

public class Task_3_6 {

    public void main() {

        int x = 1;

        while (x <= 20) {

            System.out.println(x);
            x++;
        }
    }
}
