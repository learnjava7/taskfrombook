package chapter_3;

/**
 * Napisz program, który za pomocą instrukcji for wyświetla
 * liczby całkowite od 1 do 20.
 **/

public class Task_3_4 {

    public void main() {


        for (int i = 1; i <= 20; i++) {
            System.out.println(i);
        }
    }

}

