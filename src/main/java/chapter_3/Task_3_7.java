package chapter_3;

/**
 * Napisz program, który przy użyciu instrukcji for sumuje
 * liczby całkowite od 1 do 100.
 **/

public class Task_3_7 {

    public void main() {

        int sum = 0;


        for (int i = 1; i <= 100; i++) {

            sum += i;
        }
        System.out.println("suma: " + sum);
    }
}
