package chapter_3;

/**
 * Napisz program, który za pomocą instrukcji do ... while dla
 * danych wartości x zmieniających się od 0 do 10 oblicza wartość
 * funkcji y = 3x.
 **/
public class Task_3_2 {

    public void main() {

        int x = 0;
        int y = 0;

        do {
            y = 3 * x;
            System.out.print("x = " + x);
            System.out.println(" y = " + 3 * x);
            x++;

        } while (x <= 10);


    }
}


