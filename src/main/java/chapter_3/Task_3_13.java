package chapter_3;

/**
 * Napisz program, który za pomocą instrukcji for sumuje liczby
 * nieparzyste z przedziału od 1 do 100.
 */
public class Task_3_13 {

    public void main() {

        int sum = 0;

        for (int i = 1; i <= 100; i++) {

            if (i % 2 == 1) {
                sum += i;
            }
        }
        System.out.println("suma: " + sum);
    }
}
