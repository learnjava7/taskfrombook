package chapter_3;

/**
 * Napisz program, który przy wykorzystaniu instrukcji while
 * sumuje liczby całkowite od 1 do 100.
 */
public class Task_3_9 {

    public void main() {

        int i = 1;
        int sum = 0;

        while (i <= 100) {
            sum += i;
            i++;
        }
        System.out.println("suma: " + sum);
    }
}
