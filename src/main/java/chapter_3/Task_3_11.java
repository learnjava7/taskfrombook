package chapter_3;

/**
 * Napisz program, który za pomocą instrukcji do ... while sumuje
 * liczby parzyste z przedziału od 1 do 100.
 */
public class Task_3_11 {

    public void main() {

        int i = 1;
        int sum = 0;

        do {
            if (i % 2 == 0) {
                sum += i;
            }
            i++;
        } while (i <= 100);

        System.out.println("suma: " + sum);
    }
}
